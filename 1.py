def hitungGaji(nama, gaji, bulan, dapatTunjangan):
    tunjangan = 0
    bpjs = int(gaji*3/100)
    pajak = int(gaji*5/100)
    print("===============================")
    print("Nama Karyawan: ",nama)
    print("Gaji Pokok: ",gaji)
    if(dapatTunjangan):
        print("Tunjangan: ",500000)
        tunjangan = 500000
    print("BPJS: ", bpjs)
    print("Pajak: ",pajak)
    print("===============================")
    gajiBersih = gaji+tunjangan - (bpjs + pajak)
    print("Gaji bersih: ",gajiBersih,"/ bulan")
    print("===============================")
    print("Total Gaji: ",gajiBersih*bulan)
    
    
hitungGaji("Andi", 1500000, 2, True)